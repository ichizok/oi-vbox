#!/bin/bash
set -e

declare -r WORKROOT=$(cd "${0%/*}"; pwd)
declare -r COMMAND=$1
shift

declare -A color
if [[ -t 1 ]]; then
    color=( \
        ['gray']="\e[1;30m" \
        ['red']="\e[1;31m" \
        ['green']="\e[1;32m" \
        ['yellow']="\e[1;33m" \
        ['blue']="\e[1;34m" \
        ['magenta']="\e[1;35m" \
        ['cyan']="\e[1;36m" \
        ['white']="\e[1;37m" \
        ['reset']="\e[m" \
        )
fi

P() {
    echo -e "${color['green']}==========================================================="
    echo -e "$@"
    echo -e "===========================================================${color['reset']}"
}

assert() {
    if [[ $# -gt 0 ]]; then
        echo -e "${color['red']}$@${color['reset']}" >&2
    fi
    exit 1
}

usage() {
    cat <<EOT
usage: ${0##*/} <command> [argument]
command:
    download <version>
    install <version>
    uninstall
EOT
    exit 0
}

validate() {
    [[ $# -eq 1 ]] || assert "Missing version string"
    [[ "$1" =~ ^([0-9]+\.)+[0-9]+$ ]] || assert "Invalid version format"
}

is_vbox_installed() {
    type VBoxManage >/dev/null 2>&1
}

confirm() {
    read -p "$1"
    [[ "${REPLY}" =~ ^[yY]([eE][sS])?$ ]]
}

install_pkg() {
    local pkgfile=$1

    pkgtrans "${pkgfile}" . all

    (
    cd SUNWvbox
    sed -i -e 's/PKG_MISSING_IPS=/X&/' -e 's/PKG_MISSING_SVR4=/X&/' install/checkinstall
    sed -i -e 's/^1 i checkinstall .*$/1 i checkinstall '"$("${WORKROOT}/script/cksum.py" install/checkinstall)/" pkgmap
    )

    pfexec pkgadd -d . SUNWvbox

    rm -fr SUNWvbox
}

declare -a running_vms=()
save_running_vms() {
    local vm=
    running_vms=()
    while read; do
        vm=${REPLY//\"/}
        running_vms=("${running_vms[@]}" "${vm}")
        echo "${vm}"
    done <<<"$(VBoxManage list runningvms | cut -d' ' -f1)"
}

vbox_download() {
    local dlurl=https://download.virtualbox.org/virtualbox
    local version=$1

    validate "${version}"

    local revision=$(curl -s "${dlurl}/${version}" | grep -o 'VirtualBox-'${version//./\\.}'-.\+-SunOS\.tar\.gz' | awk -F- '{print$3}')

    mkdir -p "${WORKROOT}/${version}"
    cd "${WORKROOT}/${version}"

    P "Download VirtualBox and Extpack"

    wget "https://download.virtualbox.org/virtualbox/${version}/VirtualBox-${version}-${revision}-SunOS.tar.gz"
    wget "https://download.virtualbox.org/virtualbox/${version}/Oracle_VirtualBox_Extension_Pack-${version}-${revision}.vbox-extpack"

    P "Extract VirtualBox"

    tar xf "VirtualBox-${version}-${revision}-SunOS.tar.gz"
}

vbox_install() {
    local version=$1

    validate "${version}"

    [[ -d "${WORKROOT}/${version}" ]] || assert "No ${version} directory."
    cd "${WORKROOT}/${version}"

    local revision=$(ls -1 VirtualBox-"${version}"-*-SunOS.tar.gz | tail -n1 | cut -d- -f3)
    local pkgfile="$(tar tf "VirtualBox-${version}-${revision}-SunOS.tar.gz" | grep '^VirtualBox-.*\.pkg$')"

    if [[ ! -f "${pkgfile}" ]]; then
        { ! confirm "No ${version} package. Do you want to download it? [y/N]: " \
                || ! vbox_download "${version}"; } \
            && assert "You have to download VirtualBox."
    fi
    if is_vbox_installed; then
        { ! confirm "VirtualBox has been installed. Do you want to uninstall it? [y/N]: " \
                || ! vbox_uninstall; } \
            && assert "You have to uninstall old VirtualBox."
    fi

    P "Install VirtualBox"

    install_pkg "${pkgfile}"

    P "Install Extpack"

    pfexec VBoxManage extpack install "Oracle_VirtualBox_Extension_Pack-${version}-${revision}.vbox-extpack"

    if [[ -n "${running_vms[@]}" ]]; then
        P "Start VMs"

        for vm in "${running_vms[@]}"; do
            echo "> VBoxManage startvm "${vm}" --type headless"
            VBoxManage startvm "${vm}" --type headless
        done
    fi
}

vbox_uninstall() {
    is_vbox_installed || assert "SUNWvbox is not installed"

    save_running_vms

    if [[ -n "${running_vms[@]}" ]]; then
        P "Stop running VMs"

        for vm in "${running_vms[@]}"; do
            echo "> VBoxManage controlvm ${vm} savestate"
            VBoxManage controlvm "${vm}" savestate
        done
    fi

    P "Uninstall Extpack"

    pfexec VBoxManage extpack uninstall "Oracle VirtualBox Extension Pack"

    P "Uninstall SUNWvbox"

    pfexec pkgrm SUNWvbox 
}

case "${COMMAND}" in
    download|install|uninstall)
        vbox_"${COMMAND}" "$@"
        ;;
    *)
        usage
        ;;
esac
