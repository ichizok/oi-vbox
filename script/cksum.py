#!/usr/bin/env python3

import os
import sys

def compute_checksum(path):
    from functools import reduce

    stat = os.stat(path)
    fsize = stat.st_size
    tstamp = int(stat.st_mtime)
    with open(path, "rb") as f:
        s = reduce(lambda a, b: a + b, f.read(), 0)
    bs = s.to_bytes(4, sys.byteorder)
    s = int.from_bytes(bs[:2], sys.byteorder) + int.from_bytes(bs[2:], sys.byteorder)
    bs = s.to_bytes(4, sys.byteorder)
    cksum = int.from_bytes(bs[:2], sys.byteorder) + int.from_bytes(bs[2:], sys.byteorder)
    return fsize, cksum, tstamp

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} file", sys.argv[0])
        sys.exit(1)
    fsize, cksum, tstamp = compute_checksum(sys.argv[1])
    print("{} {} {}".format(fsize, cksum, tstamp))
